
let trainer =
{};

trainer.name = "Ash Ketchum";
trainer.pokemon = "Bulbasaur";
trainer.age = 10; 
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}

trainer.talk = function()
{
	console.log("Pikachu, I choose you!")
}
trainer.talk();
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();


function Pokemon(name,level)
{
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle =function(target)
	{
		console.log(this.name + " tackled "+ target.name);
		target.health -= this.attack;

		console.log(target.name + "'s health is reduced to "+ target.health);

		if (target.health <= 0)
		{
			target.faint();
		}
	};

	this.faint = function()
	{
		console.log(this.name + " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let raichu = new Pokemon("Raichu", 15);
console.log(raichu);

let magikarp = new Pokemon("Magikarp", 18);
console.log(magikarp);

raichu.tackle(pikachu);
console.log(pikachu);

magikarp.tackle(pikachu);
console.log(pikachu);