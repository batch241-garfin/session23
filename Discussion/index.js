
// Objects //

	// An object is a data type that is used to represent
	// real world objects.
	// It is a collection of related data and/or functionalities.
	// INofrmation stored in objects are represented in 
	// key:value pair.
	// Different data types may be stored in an object

	// Creating objects
	// 1. Object Initializers/ Literal Notation
	// 2. Constructor Functions

	// Object Initializers/ Literal Notation

	// This creates/ declared an object and also initializes/
	// assigns its values upon creation.
	// It already has its keys and value such as name, color,
	// weight, etc.

	// Syntax:
	// let/const objectName = 
	// {
		// keyA: valueA;
		// keyB: valueB;
	// };

let cellphone =
{
	name: "Nokia 3210",
	manufacturedDate: 1999
};

console.log("Result from creating objects using initializers/ literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log("");

	// Constructor Function

	// Creates a reusable function to create several objects
	// that have the same data structure
	// Useful for creating multiple instances/ copies of
	// an object
	// An instance is a concrete occurence of any object which
	// emphasizes on the distinct/ unique identity of it

	// Syntax:
	// function objectName(keyA, keyB)
	// {
		// this.keyA = keyA,
		// this.keyB = keyB
	// }

function laptop(name, manufacturedDate)
{
	this.name = name;
	this.manufacturedDate = manufacturedDate;
};

function student(firstName,lastName,yearOld,city)
{
	this.name = firstName + " " + lastName;
	this.age = yearOld;
	this.residence = city;
};

// The this keyword allows us to assign a new object's properties
// by associating them with values received from a constructor function

let laptopMica = new laptop("Lenovo", 2008);
console.log(laptopMica);

let laptopEric = new laptop("HP", 1990);
console.log(laptopEric);

let elaine = new student("Elaine","SJ",12,"Mars");
console.log(elaine);

let jake = new student("Jake","Lexter",10,"Sun");
console.log(jake);

// The new keyword creates an instance of an object

// Accessing Object Properties

// There are two ways to access object properties
// 1. Using the dot notation

console.log(laptopMica.name);
console.log(elaine.name);

// 2. Using the square bracket notation

console.log("Result from square bracket notation: "+ laptopMica["name"]);
console.log("Result from square bracket notation: "+ elaine["name"]);

// Initializing / Adding / Delete / Reassigning Object Properties

// Like any other variable in Javascript, objects may have their own
// properties initialized after teh object was created/declared.
// This is useful for times when an object's properties are undetermined
// at the time of beginning.

// Empty Object

console.log("Empty car object:");
let car =
{
};
	console.log(car);
	console.log("");

	car.name = "Honda Civic";
	console.log(car);

	car.driver = "Mr. Bean";
	console.log(car);

// Initializing / Adding object properties using bracket notation


car["manufactured Date"] = 2019;
console.log(car["manufactured Date"]);
console.log("Result from adding properties using square bracket notation");
console.log(car);

// Deleting Object Properties

delete car["manufactured Date"];
console.log("Result from deleting properties:");
console.log(car);

// Reassign Object Properties

car.name = "Dodge Charger R/T";
console.log("Result from reassigning properties:");
console.log(car);

// Object Methods

// A method is a function attached to an object as a property
// A method is useful for creating object for specific properties
// Similar functions of real objects, methods are defined based on
// what an object is capable of doing.


let person =
{
	name: "John",
	talk: function()
	{
		console.log("Hello, my name is " + this.name);
	}
}

console.log(person);
console.log("Result from object methods");
person.talk();

person.walk = function()
{
	console.log(this.name + " walked 25 steps forward.");
};

person.walk();

// Methods are useful for creating reusable functions that perform tasks
// related to objects

let friend =
{
	firstName: "Arjay",
	lastName: "Dala",
	address: {
		city: "Etibak",
		country: "New York"
	},
	emails: ["arjay@gmail.com","arjay@myspace.com"],

	introduce: function(classmate)
	{
		console.log("Hello! My name is "+ this.firstName + " "+ this.lastName + ". Hi! "+ classmate);
	}
}
friend.introduce("Eric");
friend.introduce("Mica");
friend.introduce("John Daniel");

//

let myPokemon =
{
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(targetPokemon)
	{
		console.log(this.name + " tackled " + targetPokemon);
		console.log(targetPokemon + "'s health is reduced to "+ " 0000");
	},

	faint: function()
	{
		console.log("Pokemon fainted.");
	}
}

console.log(myPokemon);
myPokemon.tackle("Charmander");
console.log("");

// Create pokemon characters using constructor function

function pokemon(name, level)
{
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target)
	{
		console.log(this.name + " tackled "+ target.name);
		console.log(target.name+ "'s health is now reduced to " + target.health);
	},
	this.faint = function()
	{
		console.log(this.name + "fainted");
	}
}

let pikachu = new pokemon("Pikachu",8);
let ratata = new pokemon("Ratata",8);

pikachu.tackle(ratata);

function probinsyanoCharac(characterName,powerLevel,defenseLevel,speed)
{
	this.name = characterName;
	this.power = powerLevel;
}

let lola_gets = new probinsyanoCharac("Lol Gets",500, 40);

console.log(lola_gets);

// Product

function shoppeeProduct(name, initialPrice,discountPercent)
{
	this.name = name;
	this.price = initialPrice + 200;
	this.discount = function discounts(discountPercent)
	{
		let discountedPrice = this.price - discountPercent;
		console.log("Total: " + discountedPrice);
	},
	this.stock = function(quantity)
	{
		if (quantity <= 5)
		{
			console.log("Paubos na");
		}
		else
		{
			console.log("OKhhee pa");
		}
	}
}

let iphoneX = new shoppeeProduct("IphoneX", 3000, 0, 20);
iphoneX.discount(100);
iphoneX.stock(2);